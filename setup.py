#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(name='DupliSeek',
	version='0.1.0',
	license="MIT",
	description='Duplicate image finder',
	author='Magnus Jørgensen',
	author_email='magnusmj@gmail.com',
	url='https://gitlab.com/magnusmj/dupliseek',
	entry_points={
		'console_scripts': [
			'dupliseek = dupliseek_pkg.__main__:main',
		],
		'gui_scripts': [
			'dupliseek = dupliseek_pkg.__main__:main',
		]
	},
	packages=find_packages(),
	package_data={
		'': ['*.png', '*.css'],
	},
	install_requires=[
		'pyqt5', 'opencv-python', 'numpy', 'imutils'
	]
)
